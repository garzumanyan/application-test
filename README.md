# Task Description
You should be able to start the example application by executing ee.devolon.NeocardServerApplicantTestApplication, which starts a webserver on port 8080 (http://localhost:8080) and serves SwaggerUI where you can inspect and try existing endpoints.


You should be aware of the following conventions while you are working on this exercise:

 * All new entities should have an ID with type of Long and a date_created with type of ZonedDateTime.
 * The architecture of the web service is built with the following components:
   * DataTransferObjects: Objects which are used for outside communication via the API
   * Controller: Implements the processing logic of the web service, parsing of parameters and validation of inputs and outputs.
   * Service: Implements the business logic and handles the access to the DataAccessObjects.
   * DataAccessObjects: Interface for the database. Inserts, updates, deletes and reads objects from the database.
   * DomainObjects: Functional Objects which might be persisted in the database.

You should make a git repository (perhaps bitbucket will be good choice) and commit as frequent as you can then, as a submission share your code with me vostan.azatyan@neocard.fi.

## Task 1
 * Write a new Controller for cards.
   * Decide on your own how the methods should look like.
   * Entity Card: Should have at least the following characteristics: pan(primary account number), expiration_date, type (Visa, MasterCard, ...)
 * Extend the UserController to enable users to select a card.
 * Extend the UserController to enable users to deselect a card.
 * Extend the UserDO to map the selected card to the user.
 
## Task 2
A card can be selected by exactly one ONLINE User. If a second user tries to select a already used card you should throw a CardAlreadyInUseException.

## Task 3
Implement an endpoint in the UserController to get a list of users with specific characteristics. Reuse the characteristics you implemented in task 1.

## Task 4
Security: secure the API. It's up to you how you are going to implement the security.

## Task 5
All your code should be covered with tests, use any framework you wish.

#### NOTE
It is important to polish all existing code, any additional corrections improvements of the Service would be appreciated.
