package ee.devolon.domainobject;

import ee.devolon.converter.YearMonthConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.YearMonth;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(
        name = "card",
        uniqueConstraints = @UniqueConstraint(name = "uc_pan", columnNames = {"pan"})
)
public class CardDO extends BaseDO {

    @Column(nullable = false)
    @NotNull(message = "Primary account number can not be null!")
    private String pan;

    @Column(nullable = false)
    @NotNull(message = "Card type can not be null!")
    private String cardType;

    @Convert(converter = YearMonthConverter.class)
    @Column(nullable = false)
    @DateTimeFormat(pattern = "MM-yy", iso = DateTimeFormat.ISO.NONE)
    private YearMonth expirationDate;

    @Column(nullable = false)
    @NotNull(message = "Selected can not be null!")
    private Boolean selected = false;


    private CardDO() {
    }


    public CardDO(String pan, String cardType, YearMonth expirationDate) {
        this.pan = pan;
        this.cardType = cardType;
        this.expirationDate = expirationDate;
    }
}
