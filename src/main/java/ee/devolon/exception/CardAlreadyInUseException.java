package ee.devolon.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Card with the given id already selected.")
public class CardAlreadyInUseException extends Exception {
    private static final long serialVersionUID = -5369440333759675479L;

    public CardAlreadyInUseException(String message) {
        super(message);
    }
}
