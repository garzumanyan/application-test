package ee.devolon.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Could not find online user with given id.")
public class OnlineUserNotFoundException extends Exception {
    private static final long serialVersionUID = -1691274068666368743L;

    public OnlineUserNotFoundException(String message) {
        super(message);
    }
}