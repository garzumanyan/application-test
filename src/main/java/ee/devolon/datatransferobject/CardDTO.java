package ee.devolon.datatransferobject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CardDTO {
    @JsonIgnore
    private Long id;

    @NotNull(message = "Primary account number can not be null!")
    private String pan;

    @NotNull(message = "Card type can not be null!")
    private String cardType;

    @NotNull(message = "Expiration date can not be null!")
    private String expirationDate;

    private CardDTO() {
    }


    private CardDTO(Long id, String pan, String cardType, String expirationDate) {
        this.id = id;
        this.pan = pan;
        this.cardType = cardType;
        this.expirationDate = expirationDate;
    }


    public static CardDTOBuilder newBuilder() {
        return new CardDTOBuilder();
    }


    @JsonProperty
    public Long getId() {
        return id;
    }

    public String getPan() {
        return pan;
    }

    public String getCardType() {
        return cardType;
    }

    public String getExpirationDate() {
        return expirationDate;
    }


    public static class CardDTOBuilder {
        private Long id;
        private String pan;
        private String cardType;
        private String expirationDate;

        public CardDTOBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public CardDTOBuilder setPan(String pan) {
            this.pan = pan;
            return this;
        }

        public CardDTOBuilder setCardType(String cardType) {
            this.cardType = cardType;
            return this;
        }

        public CardDTOBuilder setExpirationDate(String expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public CardDTO createCardDTO() {
            return new CardDTO(id, pan, cardType, expirationDate);
        }
    }
}
