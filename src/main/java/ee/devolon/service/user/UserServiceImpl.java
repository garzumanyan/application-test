package ee.devolon.service.user;

import ee.devolon.dataaccessobject.UserRepository;
import ee.devolon.domainobject.CardDO;
import ee.devolon.domainobject.UserDO;
import ee.devolon.domainvalue.OnlineStatus;
import ee.devolon.exception.CardAlreadyInUseException;
import ee.devolon.exception.ConstraintsViolationException;
import ee.devolon.exception.EntityNotFoundException;

import ee.devolon.exception.OnlineUserNotFoundException;
import ee.devolon.service.card.CardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service to encapsulate the link between DAO and controller and to have business logic for some user specific things.
 * <p/>
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;
    private final CardService cardService;


    public UserServiceImpl(final UserRepository userRepository, final CardService cardService) {
        this.userRepository = userRepository;
        this.cardService = cardService;
    }


    /**
     * Selects a user by id.
     *
     * @param userId - id of a user
     * @return found user
     * @throws EntityNotFoundException if no user with the given id was found.
     */
    @Override
    public UserDO find(Long userId) throws EntityNotFoundException {
        return findCheckedUser(userId);
    }


    /**
     * Creates a new user.
     *
     * @param userDO - user data object
     * @return user created
     * @throws ConstraintsViolationException if a user already exists with the given username, ... .
     */
    @Override
    public UserDO create(UserDO userDO) throws ConstraintsViolationException {
        UserDO user;
        try {
            user = userRepository.save(userDO);
        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Some constraints are thrown due to user creation", e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return user;
    }


    /**
     * Deletes an existing user by id.
     *
     * @param userId - id of a user
     * @throws EntityNotFoundException if no user with the given id was found.
     */
    @Override
    @Transactional
    public void delete(Long userId) throws EntityNotFoundException {
        UserDO userDO = findCheckedUser(userId);
        userDO.setDeleted(true);
        userRepository.save(userDO);
    }

    /**
     * Change status of an existing user by id.
     *
     * @param userId - id of a user
     * @param onlineStatus - online status of a user
     * @throws EntityNotFoundException if no user with the given id was found.
     */
    @Override
    public void updateStatus(long userId, OnlineStatus onlineStatus) throws EntityNotFoundException {
        UserDO userDO = findCheckedUser(userId);
        userDO.setOnlineStatus(onlineStatus);
        userRepository.save(userDO);
    }


    /**
     * Find all users by online state.
     *
     * @param onlineStatus - online status of a user
     */
    @Override
    public List<UserDO> find(OnlineStatus onlineStatus) {
        return userRepository.findByOnlineStatusAndDeletedFalse(onlineStatus);
    }


    private UserDO findCheckedUser(Long userId) throws EntityNotFoundException {
        return userRepository
                .findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("Could not find entity with id: " + userId));
    }

    private UserDO findOnlineUser(Long userId) throws OnlineUserNotFoundException {
        Optional<UserDO> userDO = userRepository.findById(userId);

        if (!userDO.isPresent() || userDO.get().getDeleted() || userDO.get().getOnlineStatus() == OnlineStatus.OFFLINE) {
            throw new OnlineUserNotFoundException("Could not find online user with id: " + userId);
        }

        return userDO.get();
    }

    /**
     * Select card.
     *
     * @param userId - id of a user
     * @param cardId - id of a card
     * @throws EntityNotFoundException if no card with the given id was found
     * @throws OnlineUserNotFoundException if no online user with the given id was found
     * @throws CardAlreadyInUseException if card with the given id was already selected
     */
    @Override
    @Transactional
    public UserDO selectCard(Long userId, Long cardId) throws EntityNotFoundException, OnlineUserNotFoundException, CardAlreadyInUseException {
        UserDO user = findOnlineUser(userId);
        CardDO card = cardService.find(cardId);

        if (card.getSelected()) {
            throw new CardAlreadyInUseException("Card with id: " + cardId + "already selected");
        }

        card.setSelected(true);
        user.getCards().add(card);
        userRepository.save(user);
        return user;
    }

    /**
     * Deselect card.
     *
     * @param userId - id of a user
     * @param cardId - id of a card
     * @throws EntityNotFoundException if no user with the given id was found or if no card with the given id was found
     */
    @Override
    @Transactional
    public UserDO deselectCard(Long userId, Long cardId) throws EntityNotFoundException, OnlineUserNotFoundException {
        UserDO user = findOnlineUser(userId);
        CardDO card = cardService.find(cardId);

        card.setSelected(false);
        user.getCards().remove(card);
        userRepository.save(user);
        return user;
    }
}
