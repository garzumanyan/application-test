package ee.devolon.service.user;

import ee.devolon.domainobject.UserDO;
import ee.devolon.domainvalue.OnlineStatus;
import ee.devolon.exception.CardAlreadyInUseException;
import ee.devolon.exception.ConstraintsViolationException;
import ee.devolon.exception.EntityNotFoundException;
import ee.devolon.exception.OnlineUserNotFoundException;

import java.util.List;

public interface UserService {

    UserDO find(Long userId) throws EntityNotFoundException;

    UserDO create(UserDO userDO) throws ConstraintsViolationException;

    void delete(Long userId) throws EntityNotFoundException;

    void updateStatus(long userId, OnlineStatus onlineStatus) throws EntityNotFoundException;

    List<UserDO> find(OnlineStatus onlineStatus);

    UserDO selectCard(Long userId, Long cardId) throws EntityNotFoundException, OnlineUserNotFoundException, CardAlreadyInUseException;

    UserDO deselectCard(Long userId, Long cardId) throws EntityNotFoundException, OnlineUserNotFoundException;

}
