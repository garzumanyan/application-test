package ee.devolon.service.card;

import ee.devolon.dataaccessobject.CardRepository;
import ee.devolon.domainobject.CardDO;
import ee.devolon.exception.ConstraintsViolationException;
import ee.devolon.exception.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;


/**
 * Service to encapsulate the link between DAO and controller and to have business logic for some user specific things.
 * <p/>
 */
@Service
public class CardServiceImpl implements CardService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CardServiceImpl.class);

    private final CardRepository cardRepository;


    public CardServiceImpl(final CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }


    /**
     * Selects a user by id.
     *
     * @param cardId - id of a card
     * @return found card
     * @throws EntityNotFoundException if no card with the given id was found.
     */
    @Override
    public CardDO find(Long cardId) throws EntityNotFoundException {
        return findCheckedCard(cardId);
    }


    /**
     * Creates a new card.
     *
     * @param cardDO - card data object
     * @return card created
     * @throws ConstraintsViolationException if a card already exists with the given pan, ... .
     */
    @Override
    public CardDO create(CardDO cardDO) throws ConstraintsViolationException {
        CardDO card;

        try {
            card = cardRepository.save(cardDO);
        } catch (DataIntegrityViolationException e) {
            LOGGER.warn("Some constraints are thrown due to card creation", e);
            throw new ConstraintsViolationException(e.getMessage());
        }

        return card;
    }


    private CardDO findCheckedCard(Long cardId) throws EntityNotFoundException {
        return cardRepository
                .findById(cardId)
                .orElseThrow(() -> new EntityNotFoundException("Could not find entity with id: " + cardId));
    }

}
