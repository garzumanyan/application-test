package ee.devolon.service.card;

import ee.devolon.domainobject.CardDO;
import ee.devolon.exception.ConstraintsViolationException;
import ee.devolon.exception.EntityNotFoundException;


public interface CardService {

    CardDO find(Long cardId) throws EntityNotFoundException;

    CardDO create(CardDO userDO) throws ConstraintsViolationException;

}
