package ee.devolon.domainvalue;

public enum OnlineStatus {
    ONLINE, OFFLINE
}
