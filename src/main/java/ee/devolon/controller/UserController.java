package ee.devolon.controller;

import ee.devolon.controller.mapper.UserMapper;
import ee.devolon.datatransferobject.UserDTO;
import ee.devolon.domainobject.UserDO;
import ee.devolon.domainvalue.OnlineStatus;
import ee.devolon.exception.CardAlreadyInUseException;
import ee.devolon.exception.ConstraintsViolationException;
import ee.devolon.exception.EntityNotFoundException;
import ee.devolon.exception.OnlineUserNotFoundException;
import ee.devolon.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * All operations with a user will be routed by this controller.
 * <p/>
 */
@RestController
@RequestMapping("v1/users")
public class UserController {

    private final UserService userService;


    @Autowired
    public UserController(final UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/{userId}")
    public UserDTO getUser(@Valid @PathVariable long userId) throws EntityNotFoundException {
        return UserMapper.makeUserDTO(userService.find(userId));
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserDTO createUser(@Valid @RequestBody UserDTO userDTO) throws ConstraintsViolationException {
        UserDO userDO = UserMapper.makeUserDO(userDTO);
        return UserMapper.makeUserDTO(userService.create(userDO));
    }


    @DeleteMapping("/{userId}")
    public void deleteUser(@Valid @PathVariable long userId) throws EntityNotFoundException {
        userService.delete(userId);
    }

    @GetMapping
    public List<UserDTO> findUsers(@RequestParam OnlineStatus onlineStatus)
            throws ConstraintsViolationException, EntityNotFoundException {
        return UserMapper.makeUserDTOList(userService.find(onlineStatus));
    }

    @PostMapping("/select/{userId}")
    public UserDTO selectCard(@Valid @PathVariable long userId, @Valid @RequestBody long cardId) throws EntityNotFoundException, OnlineUserNotFoundException, CardAlreadyInUseException {
        UserDO userDO = userService.selectCard(userId, cardId);
        return UserMapper.makeUserDTO(userDO);
    }

    @PostMapping("/deselect/{userId}")
    public UserDTO deselectCard(@Valid @PathVariable long userId, @Valid @RequestBody long cardId) throws EntityNotFoundException, OnlineUserNotFoundException {
        UserDO userDO = userService.deselectCard(userId, cardId);
        return UserMapper.makeUserDTO(userDO);
    }

}
