package ee.devolon.controller.mapper;

import ee.devolon.datatransferobject.CardDTO;
import ee.devolon.domainobject.CardDO;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class CardMapper {
    public static CardDO makeCardDO(CardDTO cardDTO) {
        return new CardDO(
                cardDTO.getPan(),
                cardDTO.getCardType(),
                YearMonth.parse(cardDTO.getExpirationDate(), DateTimeFormatter.ofPattern("MM-yy"))
        );
    }


    public static CardDTO makeCardDTO(CardDO cardDO) {
        String parsedExpirationDate = cardDO.getExpirationDate().getMonth() + "-" + cardDO.getExpirationDate().getYear();
        CardDTO.CardDTOBuilder cardDTOBuilder = CardDTO.newBuilder()
                .setId(cardDO.getId())
                .setPan(cardDO.getPan())
                .setCardType(cardDO.getCardType())
                .setExpirationDate(parsedExpirationDate);

        return cardDTOBuilder.createCardDTO();
    }


    public static List<CardDTO> makeCardDTOList(Collection<CardDO> cards) {
        return cards.stream()
                .map(CardMapper::makeCardDTO)
                .collect(Collectors.toList());
    }
}
