package ee.devolon.controller.mapper;

import ee.devolon.datatransferobject.UserDTO;
import ee.devolon.domainobject.UserDO;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class UserMapper {
    public static UserDO makeUserDO(UserDTO userDTO) {
        return new UserDO(userDTO.getUsername(), userDTO.getPassword());
    }


    public static UserDTO makeUserDTO(UserDO userDO) {
        UserDTO.UserDTOBuilder userDTOBuilder = UserDTO.newBuilder()
                .setId(userDO.getId())
                .setPassword(userDO.getPassword())
                .setUsername(userDO.getUsername());

        return userDTOBuilder.createUserDTO();
    }


    public static List<UserDTO> makeUserDTOList(Collection<UserDO> users) {
        return users.stream()
                .map(UserMapper::makeUserDTO)
                .collect(Collectors.toList());
    }
}
