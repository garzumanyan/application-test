package ee.devolon.controller;

import ee.devolon.controller.mapper.CardMapper;
import ee.devolon.datatransferobject.CardDTO;
import ee.devolon.domainobject.CardDO;
import ee.devolon.exception.ConstraintsViolationException;
import ee.devolon.exception.EntityNotFoundException;
import ee.devolon.service.card.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * All operations with a card will be routed by this controller.
 * <p/>
 */
@RestController
@RequestMapping("v1/cards")
public class CardController {

    private final CardService cardService;


    @Autowired
    public CardController(final CardService cardService) {
        this.cardService = cardService;
    }


    @GetMapping("/{cardId}")
    public CardDTO getCard(@Valid @PathVariable long cardId) throws EntityNotFoundException {
        return CardMapper.makeCardDTO(cardService.find(cardId));
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CardDTO createCard(@Valid @RequestBody CardDTO cardDTO) throws ConstraintsViolationException {
        CardDO cardDO = CardMapper.makeCardDO(cardDTO);
        return CardMapper.makeCardDTO(cardService.create(cardDO));
    }
}
