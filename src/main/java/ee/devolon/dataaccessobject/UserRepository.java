package ee.devolon.dataaccessobject;

import ee.devolon.domainobject.UserDO;
import ee.devolon.domainvalue.OnlineStatus;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Database Access Object for user table.
 * <p/>
 */
public interface UserRepository extends CrudRepository<UserDO, Long> {

    List<UserDO> findByOnlineStatus(OnlineStatus onlineStatus);

    List<UserDO> findByOnlineStatusAndDeletedFalse(OnlineStatus onlineStatus);
}
