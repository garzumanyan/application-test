package ee.devolon.dataaccessobject;

import ee.devolon.domainobject.CardDO;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Database Access Object for card table.
 * <p/>
 */
public interface CardRepository extends CrudRepository<CardDO, Long> {

    List<CardDO> findByPan(String pan);

    List<CardDO> findByCardType(String cardType);
}
