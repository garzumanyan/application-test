/**
 * CREATE Script for init of DB
 */

-- Create 3 OFFLINE users

insert into user (id, date_created, deleted, online_status, password, username) values (1, now(), false, 'OFFLINE',
'user01pw', 'user01');

insert into user (id, date_created, deleted, online_status, password, username) values (2, now(), false, 'OFFLINE',
'user02pw', 'user02');

insert into user (id, date_created, deleted, online_status, password, username) values (3, now(), false, 'OFFLINE',
'user03pw', 'user03');


-- Create 3 ONLINE users

insert into user (id, date_created, deleted, online_status, password, username) values (4, now(), false, 'ONLINE',
'user04pw', 'user04');

insert into user (id, date_created, deleted, online_status, password, username) values (5, now(), false, 'ONLINE',
'user05pw', 'user05');

insert into user (id, date_created, deleted, online_status, password, username) values (6, now(), false, 'ONLINE',
'user06pw', 'user06');